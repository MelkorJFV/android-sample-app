package melkor.capacitacion.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import melkor.capacitacion.DataModel.WeatherDetailsAdapter;
import melkor.capacitacion.R;
import melkor.capacitacion.Utils.DataHandler;

public class DetailsActivity extends AppCompatActivity {

    WeatherDetailsAdapter itemAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ListView listView = (ListView)this.findViewById(R.id.detailsList);
        itemAdapter = new WeatherDetailsAdapter(this, R.layout.details_row, DataHandler.details);
        listView.setAdapter(itemAdapter);
    }
}
