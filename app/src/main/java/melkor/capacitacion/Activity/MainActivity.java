package melkor.capacitacion.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Vector;

import melkor.capacitacion.DataModel.WeatherDetails;
import melkor.capacitacion.R;
import melkor.capacitacion.Utils.AsynkConnector;
import melkor.capacitacion.Utils.Callback;
import melkor.capacitacion.Utils.DataHandler;

public class MainActivity extends AppCompatActivity {


    boolean isKelvin = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Switch stc = (Switch)findViewById(R.id.homeSwitch);
        stc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                doSwitchChange();
            }
        });

        getData();

        Button map = (Button)findViewById(R.id.mapBtn);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToMap();
            }
        });

        Button details = (Button)findViewById(R.id.detailsBtn);
        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToDetails();
            }
        });
    }

    private void doSwitchChange() {

        try{

            DecimalFormat df = new DecimalFormat("#.00");

            if (isKelvin){

                Double t = new Double(((TextView)findViewById(R.id.temperature)).getText().toString());
                t = t-273.15;
                ((TextView)findViewById(R.id.temperature)).setText(df.format(t));

                t = new Double(((TextView)findViewById(R.id.min)).getText().toString());
                t = t-273.15;
                ((TextView)findViewById(R.id.min)).setText(df.format(t));

                t = new Double(((TextView)findViewById(R.id.max)).getText().toString());
                t = t-273.15;
                ((TextView)findViewById(R.id.max)).setText(df.format(t));

                ((TextView)findViewById(R.id.labelTemp)).setText(R.string.temperatureC);
                ((TextView)findViewById(R.id.labelMinTemp)).setText(R.string.min_temperatureC);
                ((TextView)findViewById(R.id.labelMaxTemp)).setText(R.string.max_temperatureC);
            }else{
                Double t = new Double(((TextView)findViewById(R.id.temperature)).getText().toString());
                t = t+273.15;
                ((TextView)findViewById(R.id.temperature)).setText(df.format(t));

                t = new Double(((TextView)findViewById(R.id.min)).getText().toString());
                t = t+273.15;
                ((TextView)findViewById(R.id.min)).setText(df.format(t));

                t = new Double(((TextView)findViewById(R.id.max)).getText().toString());
                t = t+273.15;
                ((TextView)findViewById(R.id.max)).setText(df.format(t));

                ((TextView)findViewById(R.id.labelTemp)).setText(R.string.temperature);
                ((TextView)findViewById(R.id.labelMinTemp)).setText(R.string.min_temperature);
                ((TextView)findViewById(R.id.labelMaxTemp)).setText(R.string.max_temperature);
            }
            isKelvin = !isKelvin;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getData() {

        String content = "{}";

        String param = "?id=3435910";

        AsynkConnector c = new AsynkConnector(AsynkConnector.WEATHER, param ,content, new Callback() {
            private int dots = 0;
            @Override
            public void starting() {

            }

            @Override
            public void completed(String res, int responseCode) {
                if(responseCode==200){
                    parseResponseJson(res);
                }else{
                }
            }

            @Override
            public void completedWithErrors(Exception e) {
            }

            @Override
            public void update() {
            }
        });
        c.execute();
    }


    public void goToMap(){
        Intent intent = new Intent(getBaseContext(), MapsActivity.class);
        startActivity(intent);
    }

    public void goToDetails(){
        Intent intent = new Intent(getBaseContext(), DetailsActivity.class);
        startActivity(intent);
    }

    private void parseResponseJson(String res){
        try{
            JSONObject reader = new JSONObject(res);

            //coord
            JSONObject d = reader.getJSONObject("coord");
            String lon = d.getString("lon");
            String lat = d.getString("lat");

            try{
                DataHandler.longitude = new Double(lon);
                DataHandler.latitude = new Double(lat);
            }catch (Exception e){
                e.printStackTrace();
            }



            //main
            d = reader.getJSONObject("main");
            TextView temp = (TextView)findViewById(R.id.temperature);
            temp.setText( d.getString("temp"));
            TextView pressure = (TextView)findViewById(R.id.pressure);
            pressure.setText( d.getString("pressure"));
            TextView humidity = (TextView)findViewById(R.id.humidity);
            humidity.setText( d.getString("humidity"));
            TextView temp_min = (TextView)findViewById(R.id.min);
            temp_min.setText( d.getString("temp_min"));
            TextView temp_max = (TextView)findViewById(R.id.max);
            temp_max.setText( d.getString("temp_max"));


            //from general
            TextView regionCode = (TextView)findViewById(R.id.regionCode);
            regionCode.setText( reader.getString("name"));

            //sys
            d = reader.getJSONObject("sys");
            TextView country = (TextView)findViewById(R.id.countryCode);
            country.setText( d.getString("country"));


            DataHandler.location = regionCode.getText().toString() +" - "+ country.getText().toString();

            //weather
            JSONArray a = reader.getJSONArray("weather");
            DataHandler.details = new Vector<WeatherDetails>();
            for (int i = 0; i<a.length(); i++){
                JSONObject o = a.getJSONObject(i);
                WeatherDetails aux = new WeatherDetails();
                aux.setDescription(o.getString("description"));
                aux.setIcon(o.getString("icon"));

                DataHandler.details.add(aux);
            }

        }catch (JSONException e){
            System.out.println(e.getMessage());
        }

    }
}
