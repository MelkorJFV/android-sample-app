package melkor.capacitacion.DataModel;

/**
 * Created by jfvazquez on 15/9/2016.
 */
public class WeatherDetails {


    private String icon;
    private String description;



    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
