package melkor.capacitacion.DataModel;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.Vector;

import melkor.capacitacion.R;

/**
 * Created by jfvazquez on 15/9/2016.
 */
public class WeatherDetailsAdapter extends ArrayAdapter<WeatherDetails>{
    private Activity myContext;
    private Vector<WeatherDetails> data;

    public WeatherDetailsAdapter (Context context, int resourceId, Vector<WeatherDetails> planillas){
        super(context, resourceId, planillas);
        myContext = (Activity)context;
        data = planillas;
        //Collections.sort(data);



    }

    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = myContext.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.details_row, null);

        TextView details = (TextView)rowView.findViewById(R.id.details_text);
        details.setText(data.elementAt(position).getDescription());


        ImageView icon = (ImageView)rowView.findViewById(R.id.details_icon);

        if (data.elementAt(position).getIcon().equals("01d")) {
            icon.setImageResource(R.drawable.w01d);
        } else if (data.elementAt(position).getIcon().equals("01n")) {
            icon.setImageResource(R.drawable.w01n);

        }else if (data.elementAt(position).getIcon().equals("02d")) {
            icon.setImageResource(R.drawable.w02d);
        } else if (data.elementAt(position).getIcon().equals("02n")) {
            icon.setImageResource(R.drawable.w02n);

        }else if (data.elementAt(position).getIcon().equals("03d")) {
            icon.setImageResource(R.drawable.w03d);
        } else if (data.elementAt(position).getIcon().equals("03n")) {
            icon.setImageResource(R.drawable.w03n);

        }else if (data.elementAt(position).getIcon().equals("04d")) {
            icon.setImageResource(R.drawable.w04d);
        } else if (data.elementAt(position).getIcon().equals("04n")) {
            icon.setImageResource(R.drawable.w04n);

        }else if (data.elementAt(position).getIcon().equals("09d")) {
            icon.setImageResource(R.drawable.w09d);
        } else if (data.elementAt(position).getIcon().equals("09n")) {
            icon.setImageResource(R.drawable.w09n);

        }else if (data.elementAt(position).getIcon().equals("10d")) {
            icon.setImageResource(R.drawable.w10d);
        } else if (data.elementAt(position).getIcon().equals("10n")) {
            icon.setImageResource(R.drawable.w10n);

        }else if (data.elementAt(position).getIcon().equals("11d")) {
            icon.setImageResource(R.drawable.w11d);
        } else if (data.elementAt(position).getIcon().equals("11n")) {
            icon.setImageResource(R.drawable.w11n);

        }else if (data.elementAt(position).getIcon().equals("13d")) {
            icon.setImageResource(R.drawable.w13d);
        } else if (data.elementAt(position).getIcon().equals("13n")) {
            icon.setImageResource(R.drawable.w13n);

        }else if (data.elementAt(position).getIcon().equals("50d")) {
            icon.setImageResource(R.drawable.w50d);
        } else if (data.elementAt(position).getIcon().equals("50n")) {
            icon.setImageResource(R.drawable.w50n);
        }

        if (position % 2 == 1) {
            rowView.setBackgroundColor(Color.LTGRAY);
        }

        return rowView;
    }

    public void updateData(Vector<WeatherDetails> listData) {
        this.data = listData;
    }


}
