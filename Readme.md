**Android**
============================
En el siguiente documento se mostrara el paso a paso para armar una aplicación en Android Java.
Se aclara que es Java, por que Android Nativo se puede interpretar como C++

[TOC]


TODO
--------------------------------------------------------------------
 - Code Clean Up
 - Evolve Sample


Aplicacion de ejemplo
--------------------------------------------------------------------
La aplicacion de ejemplo se conectara a los servicios de [OpenWeatherMaps](https://openweathermap.org/) via Get, y recuperara el [Json](http://www.json.org/). Con los datos recuperados poblaremos la pantalla.

Requerimientos Previos
--------------------------------------------------------------------
Para empezar, y como un basico necesitamos bajar el [Android Studio](https://developer.android.com/studio/index.html). 


Creando el Proyecto
--------------------------------------------------------------------
Seleccionar dentro de ***File ->New -> New Project***

 - **New Project:** Llamaremos nuestra aplicacion *"AndroidSampleApp"* por que eso es lo que es.
 - **Target Android Devices:** Siguiente paso es seleccionar el trget de nuestro proyecto y el [API level](https://en.wikipedia.org/wiki/Android_version_history) correspondiente. Seleccionaremos solamente *Phone and Tablet* y en el campo de *Minimum SDK* eligiremos *API15: Android 4.0.3*
 - **Add an Activity to Mobile:** Seleccionamos *Empty Activity* que basicamente os generara un Activity que en el layout solamente va a tener un Label con el texto *Hello World*
 - **Customize the Activity:** Aqui debemos poner el nombre de la Activity, al escribir en el campo superior sugerira automaticamente el nombre del layout relacionado. Por convencion respetar la siguiente nomenclatura *NombreActivity*, en este caso MainActivity, dado que es la Activity principal.


**Estructura del Proyecto**

El proyecto en su vista default como se abre presenta la siguiente estructura:


 - **app** *- Esta carpeta es la basica que engloba al proyecto en si.*
	 - **manifests** *- Aqui encontraremos el Manifest correspondiente a la aplicacion*
	 - **java**
		 - **vendor.appName** *- Aqui encontraremos el codigo de nuestra app*
		 - **vendor.appName (androidTest)** *- Utilizado para testing en device o emulador*
		 - **vendor.appName (test)** *- Utilizado para Unit Testing*
	 - **res** *- Carpeta donde se encontraran todos los recursos*
		 - **drawable** *- Aqui es donde iran los recursos independientes de DPI*
		 - **layout** *- Aqui estaran los xml de layouting*
		 - **mipmap** *- Recursos dependientes de DPI de pantalla*
		 - **values** *- Aqui encontraremos xml de definicion de strings, dimenciones, etc tanto dependientes como independientes de DPI*
			 - dimens.xml
 - **Gradle Scripts** *- Desde la migracion de Eclipse a IntelliJ el building se hace por medio de Gradle, aqui se encuentran los scripts referente a esto*

Como primer paso y por una cuestion de mantener el codigo ordenado haremos, haremos un nuevo paquete dentro de **vendor.AppName** donde dejaremos todos los Activity. Hacemos entonces, boton derecho sobre el **vendor.AppName** y seleccionamos **new-> Package** y lo nombramos Activity.
Paso seguido, buscamos nuestro *MainActivity.Java* y lo movemos a este nuevo paquete, Android Studio hara una serie de prompts con respecto al refactor.


Comenzando el desarrollo
--------------------------------------

El desarrollo lo iremos haciendo desde la conexion REST hasta mostrar los datos en pantalla.
Empecemos entonces creando un nuevo paquete dentro de **vendor.AppName** que se llame **Utils** aqui dejaremos, de momento todo lo que sea generico para toda la aplicacion.

**Callback**
Dentro de Utils crearemos una Interfaz, la cual llamaremos Callback. El objetivo de la misma es que sea implementada por quien llame a la conexion REST, que es asincronica, y pueda resolver lo que necesitemos. (este punto quedara mas claro cuando estemos haciendo la coneccion REST, de momento paciencia)

```java
package melkor.capacitacion.Utils;
public interface Callback {
    public void starting();
    public void completed(String res, int responseCode);
    public void completedWithErrors(Exception e);
    public void update();
}
```

**AsynkConnector**
Ahora pasaremos a crear el conector a servicios REST. Para eso recomiendo bajen el del ejemplo e importenlo a su proyecto personal, de todas formas explicare las partes mas importantes del mismo para clarificar su funcionamiento.

```java
public class AsynkConnector extends AsyncTask<String, String, String> {
```
Esta clase extiende de *AsyncTask* esto le dara la capacidad de ser ejecutada como una tarea secundaria y en background. Un Asynk task es un Thread separado del main de la aplicacion. Para esto deberemos implementar los siguientes metodos:

```java
@Override
protected String doInBackground(String... strings) {
	return doConnection();
}
```
Aqui llamaremos al metodo que queremos corra en el thread separado sin que interrumpa el flujo de la aplicacion.
 
```java
@Override
protected void onPostExecute(String result) {
	callback.completed(result, responseCode);
}
```
Una vez finalizada la tarea lanzada en *onPostExecute* termina, este metodo es invocado. Es por eso que llamamos al completed de nuestro Callback

```java
@Override
protected void onPreExecute() {
	callback.starting();
}
```
El *AsynkTask* pasara por este punto previo a inicializarce. Si hay configuraciones o cosas que hacer previo a la ejecucion del thread, este es el lugar donde hacerlo. ATENCION que esta seccion todavia se encuentra corriendo sobre el main thread de la aplicacion.

```java
@Override
protected void onProgressUpdate(String... text) {
	callback.update();
}
``` 
Este metodo basicamente se utiliza, por ejemplo si yo quiero ir completando una barra de progreso o algo simil. Se llamara cada tanto. En nuestro caso llamaremos al update del callback.


Esta clase fue diseñada para que sea altamente configurable, y se pueda utilizar para todo tipo de conexiones REST.
Declarado como variables globales podemos encontrar lo siguiente:
```java
//Esta es la URL base donde se encontraran los servicios
private final String endpoint = "http://api.openweathermap.org/";

//Si hay un append final que haya que acoplar a todos los requests, se configura aqui, en este caso, la key para hacer el request
private final String append = "&APPID=351b0c79cd9e7d53da8b3d823e12584e";

/*Este es un int que utilizaremos a continuacion, si quiciera agregar otro servicio bajo el mismo endpoint lo agrego aqui.*/
public static final int WEATHER = 0;

/*Este es el append particular para le servicio que estaremos consultando, si hubiese otro, lo agregamos aqui.*/
private static final String weather_append = "data/2.5/weather";
```

Una vez con la parte de configuraciones completa tenemos el siguiente metodo:

```java
private String composeURL(int service, String param) {
	String ret = null;
	switch (service){
            case WEATHER:
	            /*conformo la URL utilizando params que me enviara la clase que invoque, y las variables declaradas en esta. Tambien configuramos el tipo de conexion*/
                ret = endpoint+weather_append+param+append;
                method = GET;
                break;
        }
        return ret;
    }
```    
En este caso tenemos un solo servicio pero si tuviesemos y hubiesemos declarado mas, podriamos agregar el *case* para ese nuevo servicio.

Finalmente la constructora:
```java
public AsynkConnector(int service, String param, String content, Callback callback){

        this.callback = callback;
        this.content = content;
        this.service = service;
        this.param = param;
        url = composeURL(service, param);
    }
```

recibe como parametros, las siguientes cosas:

 - *service*  - Servicio al cual querramos invocar, en este caso siempre sera WEATHER
 - *param* - Parametros adicionales.
 - *content* - En caso de ser un POST o PUT tenemos que tener un content a enviar.
 - *callback* - Es el callback que utilizaremos para mantener informada a nuestro thread principal con las cosas que iran ocurriendo en la tarea asincronica.


**MainActivity**
Volvemos entonces a nuestro *MainActivity* lo primero que haremos sera agregar un metodo que reciba como parametro un String que contiene un JSON, el cual parsearemos y convertiremos en un JSONObject, y luego usaremos este objeto para poblar los campos definidos en el xml:

```java
    private void parseResponseJson(String res){
        try{
	        //pasamos el string a JSON
            JSONObject reader = new JSONObject(res);
            
		//Recuperamos el JSON perteneciente al bloque con key "main"
            d = reader.getJSONObject("main");
            //Buscamos el TextView definido en el xml de layout
            TextView temp = (TextView)findViewById(R.id.temperature);
            /*Recuperamos el valor del key "temp" y se lo asignamos al TextView recuperado */
            temp.setText( d.getString("temp"));
     
        }catch (JSONException e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
```

Estamos a solo dos pasos de tener la aplicacion andando, el siguiente que tenemos que hacer es llamar a nuestra tarea asincronica y pedirle que recupere del servicio lo necesario, para eso haremos el siguiente metodo:

```java
private void getData() {
	//No requerimos content, por ende lo enviamos vacio
        String content = "{}";
        //El param adicional para agregar en el request, en este caso es el codigo de ciudad a recuperar
        String param = "?id=3435910";
        /*Creamos una instancia de nuestro AsynkConnector creando una instancia virtual de Callback*/
        AsynkConnector c = new AsynkConnector(AsynkConnector.WEATHER, param ,content, new Callback() {
            private int dots = 0;
            @Override
            public void starting() {
            }

            @Override
            public void completed(String res, int responseCode) {
	            /*si respondio con 200 (la conexion resulto exitosa) llamaremos al metodo previamente implementado pasandole el srting recuperado del servicio*/
                if(responseCode==200){
                    parseResponseJson(res);
                }else{
                }
            }

            @Override
            public void completedWithErrors(Exception e) {
            }

            @Override
            public void update() {
            }
        });
        //Ejecutamos la tarea asincronica
        c.execute();
    }
```


Finalmente, lo que nos queda por hacer es llamar a este metodo desde algun lugar, lo haremos agregando la invocacion en el metodo *onCreate* de nuestrto *MainActivity*

```java
 @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Agregamos el llamado a nuestro metodo
        getData();
    }

```


**Fin**
---------------------------------------------------------------------------
Finalmente tenemos nuestra aplicacion andando, ahora podremos ejecutarla y ver aparecer en el emulador o celular la temperatura en la Ciudad de Buenos Aires.

